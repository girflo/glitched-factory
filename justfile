default:
  just --list

run:
    cargo run

compile:
    cargo build --release

new-version old-version new-version:
    ./tools/release_new_version.sh {{old-version}} {{new-version}}

[macos]
package:
    cargo packager --release

[linux]
package:
    cargo packager --release

[windows]
package:
    cargo wix -v --nocapture -o .
