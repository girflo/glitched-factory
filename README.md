# Glitched factory

Create glitched version of you PNG images, available for Linux, Macos, Windows.

## More information

- [Website](https://glitchedfactory.com/)
- [Code](https://gitlab.com/girflo/glitched-factory)
- [Developer's website](https://duckpuck.net/)

