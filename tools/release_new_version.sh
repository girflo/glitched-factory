#!/usr/bin/env sh

if [ "$#" -ne 2 ]; then
    echo "Usage: $0 old_version new_version"
    exit 1
fi

sed -i '' -e "s/Unreleased/$2/g" changelog.md
sed -i '' -e "s/$1/$2/g" Cargo.toml
sed -i '' -e "s/VERSION: $1/VERSION: $2/g" .github/workflows/continuous-integration.yml
git add changelog.md Cargo.toml .github/workflows/continuous-integration.yml
git commit -m "v$2"
git tag -a "v$2" -m "version $2"
