# Changelog

## [0.4.0]

### Added

- Display input preview when input is a single image
- Ongoing tasks now show a spinner beside their name
- Task name now show the an image or directory icon depending of the input type
- Show a message in the help tab when a new version of this software is available:

### Changed

- Use default theme instead of dark one
- Remove ... from io buttons
- Change display of headers
- Increased width of queue panel
- Reduce size of subheader in help page
- Open input/output collapsing headers by default

### Fixed

- The three columns are now scrollable vertically

## [0.3.9]

### Added

- Low resolution option for dithor

### Changed

- For folder input the counter is now after the name in ouput file's name
- Update arko to version 0.2.4
- Update dithor to version 0.3.2
- Update eframe to version 0.28.1
- Update image to version 0.25.2
- Update lazy_static to version 1.5.0
- Update pnglitcher to version 0.3.2
- Update rfd to version 0.14.1

## [0.3.8]

### Added

- webp support for converter
- bmp support for converter
- Icons in input and output buttons
- Info text for overwrite output
- Info text for clear queue button
- Info text for input/output buttons
- Icon for windows binary

### Changed

- B&W option in Dithor is now Black & White

### Fixed

- The max and max2 options for sort smart are visible on small screens
- Dithor can now handle grayscale images without issue
- Display of colors for slim options on small screens

## [0.3.7]

### Added

- Pass overwrite flag to dithor
- Pass overwrite flag to PNGlitcher
- Support for transparency in Glitch algorithms

### Changed

- When the output filename already exists and the overwriting is deactivated the newly created filename has now a timestamp in it's name.

## [0.3.6]

### Fixed

- Folder input for conversion
- Dropping files remove picked one

### Changed

- The error panel is a little bit more visible
- Improve display of dropped files

### Added

- Help section for Dithor

## [0.3.5]

### Added

- A clear button for the queue (remove finished tasks)

### Changed

- The queue panels are now scrollable
- The icon background is now sorted pixels
- The output filenames are now padded with zeros on the left up to 100

## [0.3.4]

### Added

- Add Dithor to sample

### Changed

- Split algo in two categories in the main app
- Update version of egui

## [0.3.3]

### Added

- Add [Dithor algo](https://duckpuck.net/projects/dithor/)

### Fixed

- Option for variable filter algoritm
- Folder input

## [0.3.2]

### Added

- Variable filter algorithm
- Add grayscale tool

### Fixed

- Directory input

## [0.3.1]

### Added

- Id of task in the queue

### Changed

- Rewrite the entire documentation and add missing parts.
- New logo for the project

### Fixed

- Displayed order of tasks in queue
- Displayed index in output only with directory input

## [0.3.0]

All notable changes to this project will be documented in this file.

The version 0.3.x is a complete rewrite of the version 0.2.x which wwas a complete rewrite of the version 0.1.x.
I hope this will be the last rewrite for this software.

[Link to version 0.1.x](https://gitlab.com/girflo/legacy-glitched-factory)

[Link to version 0.2.x](https://gitlab.com/girflo/glitched-factory-jruby)
