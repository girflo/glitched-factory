pub fn call(filename: &str, convertion: bool) -> bool {
    let filename_lc = filename.to_lowercase();
    if convertion {
        filename_lc.ends_with(".png")
            || filename_lc.ends_with(".jpg")
            || filename_lc.ends_with(".jpeg")
            || filename_lc.ends_with(".webp")
            || filename_lc.ends_with(".bmp")
    } else {
        filename_lc.ends_with(".png")
    }
}
