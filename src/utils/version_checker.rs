use reqwest::Error;
use tokio::runtime::Runtime;
use regex::Regex;

const CARGO_FILE_URL: &str = "https://gitlab.com/png-glitch/glitched-factory/-/raw/main/Cargo.toml";

async fn check_for_update() -> UpdateStatus {
    let version_from_repository = latest_version().await;
    return UpdateStatus::new(version_from_repository);
}

async fn latest_version() -> String {
    match get_latest_version().await {
        Ok(body)  => parse_version(&body).to_string(),
        Err(_) => return "0".to_string(),
    }
}

fn parse_version(body: &str) -> &str {
    let re = Regex::new(r#"version\s*=\s*"(\d+\.\d+\.\d+)""#).unwrap();
    if let Some(caps) = re.captures(body) {
        return caps.get(1).unwrap().as_str();
    } else {
        return "0";
    }
}

async fn get_latest_version() -> Result<String, Error> {
    let response = reqwest::get(CARGO_FILE_URL).await?;
    let body = response.text().await?;
    Ok(body)
}

fn compare_versions(online_version: &str) -> bool {
    let v1: Vec<u32> = online_version.split('.').map(|s| s.parse().unwrap()).collect();
    let v2: Vec<u32> = env!("CARGO_PKG_VERSION").split('.').map(|s| s.parse().unwrap()).collect();

for (num1, num2) in v1.iter().zip(v2.iter()) {
        match num1.cmp(num2) {
            std::cmp::Ordering::Greater => return true,
            std::cmp::Ordering::Less => return false,
            std::cmp::Ordering::Equal => continue,
        }
    }

    v1.len() > v2.len()
}

pub struct UpdateStatus {
    pub new_version: bool,
    pub latest_version: String,
}

impl UpdateStatus {
    pub fn new(latest_version: String) -> UpdateStatus {
        let new_version = compare_versions(&latest_version);
        UpdateStatus {
            new_version,
            latest_version,
        }
    }
}

lazy_static! {
    pub static ref UPDATE_STATUS: UpdateStatus = {
        let rt = Runtime::new().unwrap();
        rt.block_on(check_for_update())
    };
}
