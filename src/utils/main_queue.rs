use crate::utils::Queue;
use std::sync::Mutex;

lazy_static! {
    pub static ref MAINQUEUE: Mutex<Queue> = Mutex::new(Queue::new());
}
