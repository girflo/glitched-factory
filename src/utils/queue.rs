use crate::utils::Task;
use std::collections::HashMap;
use std::sync::Arc;
use std::sync::Mutex;

#[derive(Clone, Default)]
pub struct Queue {
    tasks: Arc<Mutex<HashMap<i64, Task>>>,
}

impl Queue {
    pub fn new() -> Queue {
        Queue {
            tasks: Arc::new(Mutex::new(HashMap::new())),
        }
    }

    pub fn add_task(&mut self, id: i64, name: &str) {
        let task = Task::new(name, "Ongoing");
        self.tasks.lock().unwrap().insert(id, task);
    }

    pub fn all_sorted_tasks(&self) -> Vec<(i64, Task)> {
        let mut map2: HashMap<i64, Task> = HashMap::new();
        map2.clone_from(&self.tasks.lock().unwrap());
        let mut v: Vec<(i64, Task)> = Vec::new();
        for (key, value) in map2 {
            v.push((key, value));
        }
        v.sort_by_key(|k| k.0);
        v
    }

    pub fn finish_task(&self, id: i64) {
        self.tasks
            .lock()
            .unwrap()
            .get_mut(&id)
            .unwrap()
            .set_state("✔ Finished");
    }

    pub fn next_id(&self) -> i64 {
        self.tasks.lock().unwrap().len() as i64
    }

    pub fn clear_queue(&self) {
        let all_tasks = self.all_sorted_tasks();
        for task in all_tasks {
            if task.1.state == "✔ Finished" {
                self.tasks.lock().unwrap().remove(&task.0);
            }
        }
    }
}
