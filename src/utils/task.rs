#[derive(Clone)]
pub struct Task {
    pub name: String,
    pub state: String,
}

impl Task {
    pub fn new(name: &str, state: &str) -> Task {
        Task {
            name: name.to_string(),
            state: state.to_string(),
        }
    }

    pub fn set_state(&mut self, state: &str) {
        self.state = state.to_string();
    }
}
