use crate::utils;
use std::{fs, io};

pub fn call(folder_path: &str, convertion: bool) -> Vec<String> {
    match folder_content(folder_path, convertion) {
        Ok(v) => v,
        Err(_e) => vec![],
    }
}

fn folder_content(folder_path: &str, convertion: bool) -> io::Result<Vec<String>> {
    let mut entries = fs::read_dir(folder_path)?
        .map(|res| res.map(|e| e.path()))
        .collect::<Result<Vec<_>, io::Error>>()?;
    entries.sort();
    let mut png_files = Vec::new();
    for entry in entries {
        let file = entry.display().to_string();
        if utils::format_validator::call(&file, convertion) {
            png_files.push(file);
        }
    }
    Ok(png_files)
}
