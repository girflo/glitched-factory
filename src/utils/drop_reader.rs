use crate::utils;
use eframe::egui;
use std::path::PathBuf;

pub fn call(dropped_paths: Vec<egui::DroppedFile>, convertion: bool) -> Vec<String> {
    let mut all_files = vec![];

    for dropped_path in dropped_paths.iter() {
        let mut files = files_for_path(dropped_path.clone().path, convertion);
        all_files.append(&mut files);
    }
    all_files
}

fn files_for_path(path: Option<PathBuf>, convertion: bool) -> Vec<String> {
    if path.is_none() {
        vec![]
    } else {
        let existing_path = path.unwrap();
        if existing_path.is_file() {
            return vec![existing_path.display().to_string()];
        } else {
            utils::folder_reader::call(&existing_path.display().to_string(), convertion)
        }
    }
}
