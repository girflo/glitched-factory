pub mod spacer {
    macro_rules! medium {
        ($a:expr) => {
            $a.add_space(8.0);
        };
    }

    macro_rules! inter_io {
        ($a:expr) => {
            $a.add_space(10.0);
            $a.separator();
            $a.add_space(9.0);
        };
    }

    macro_rules! small {
        ($a:expr) => {
            $a.add_space(6.0);
        };
    }

    macro_rules! mini {
        ($a:expr) => {
            $a.add_space(3.0);
        };
    }

    pub(crate) use inter_io;
    pub(crate) use medium;
    pub(crate) use mini;
    pub(crate) use small;
}

pub mod ui_helper {
    use std::fs;
    use std::path::Path;

    macro_rules! subheader {
        ($a:expr, $b:expr) => {
            $a.label(egui::RichText::new($b).size(16.0));
        };
    }

    macro_rules! image {
        ($a:expr, $b:ident) => {
            let loaded_image = std::thread::spawn(move || {
                egui::Image::new($b)
                    .max_height(250.0)
                    .rounding(5.0)
                    .show_loading_spinner(false)
            });
            $a.add(loaded_image.join().unwrap());
            $a.add_space(6.0);
        };
    }

    macro_rules! picked_input {
        ($a:expr, $b:ident) => {
            spacer::medium!($a);
            egui::CollapsingHeader::new("Picked input")
                .default_open(true)
                .show($a, |ui| {
                    ui.label($b);
                    let image_path = format!("file://{}", $b);
                    let png_string = ui_helper::png_image_string(&$b);
                    if crate::utils::format_validator::call(&image_path, true) {
                        ui_helper::image!(ui, image_path);
                    }
                    if png_string.is_some() {
                        ui.label(png_string.unwrap());
                    }
                });
        };
    }

    pub fn png_image_string(path: &str) -> Option<String> {
        let dir = Path::new(path);
        if dir.is_dir() {
            Some(format!(
                "{} png images found in folder",
                get_images_in_directory(path)
            ))
        } else {
            None
        }
    }

    fn get_images_in_directory(path: &str) -> usize {
        let files = fs::read_dir(path).unwrap();
        files
            .filter_map(Result::ok)
            .filter(|d| {
                if let Some(e) = d.path().extension() {
                    e.to_ascii_lowercase() == "png"
                } else {
                    false
                }
            })
            .count()
    }

    pub(crate) use image;
    pub(crate) use picked_input;
    pub(crate) use subheader;
}

pub mod glitcher {

    macro_rules! glitch {
        ($a:expr, $b:expr) => {
            let id = MAINQUEUE.lock().unwrap().next_id();
            let task_name = if $a.directory_input {
                format!("🗁 {}", $b.name)
            } else {
                format!("🖻 {}", $b.name)
            };

            MAINQUEUE.lock().unwrap().add_task(id, task_name.as_str());
            let overwrite = &mut ($a.clone()).overwrite;
            let input = $a.multiple_input(false);
            let mut counter: u32 = 0;
            for i in &input {
                let string_counter: String = format!("{:0>3}", counter.to_string());
                let output = $a.multiple_output(&string_counter);
                let output_exist = Path::new(&output).exists();
                if *overwrite || !output_exist {
                    $b.call_glitch_algo(&i, &output, *overwrite);
                } else {
                    match SystemTime::now().duration_since(UNIX_EPOCH) {
                        Ok(n) => {
                            let timestamp = n.as_secs();
                            let filename_tail = format!("{}{}", timestamp, ".png");
                            let output_name = str::replace(&output, ".png", &filename_tail);
                            $b.call_glitch_algo(&i, &output_name, *overwrite);
                        }
                        Err(_) => panic!("SystemTime before UNIX EPOCH!"),
                    }
                }
                counter += 1;
            }
            MAINQUEUE.lock().unwrap().finish_task(id);
        };
    }

    pub(crate) use glitch;
}
pub mod drop_reader;
pub mod folder_reader;
pub mod format_validator;
pub mod queue;
pub use queue::Queue;
pub mod main_queue;
pub use main_queue::MAINQUEUE;
pub mod task;
pub use task::Task;
pub mod version_checker;
pub use version_checker::UPDATE_STATUS;
