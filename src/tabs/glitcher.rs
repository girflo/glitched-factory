use crate::components;
use crate::options;
use crate::utils::spacer;
use eframe::egui;

#[derive(Default)]
pub struct Glitcher {
    io_options: options::IoOptions,
    brush_options: options::BrushOptions,
    dithor_options: options::DithorOptions,
    duplicate_options: options::DuplicateOptions,
    replace_options: options::ReplaceOptions,
    slim_options: options::SlimOptions,
    sort_brut_options: options::SortBrutOptions,
    sort_smart_options: options::SortSmartOptions,
    variable_filter_options: options::VariableFilterOptions,
    wrong_filter_options: options::WrongFilterOptions,
}

impl eframe::App for Glitcher {
    fn update(&mut self, ctx: &egui::Context, _frame: &mut eframe::Frame) {
        components::io_panel::call(ctx, &mut self.io_options, true);
        components::queue_panel::call(ctx);
        components::error_panel::call(ctx, &self.io_options.error_message);

        egui::CentralPanel::default().show(ctx, |ui| {
            egui::ScrollArea::both().show(ui, |ui| {
                components::header::call(ui, "Glitch & Effects");
                egui::Frame::default().inner_margin(8.0).show(ui, |ui| {
                    components::brush::call(ui, &mut self.io_options, &mut self.brush_options);
                    spacer::small!(ui);
                    components::dithor::call(ui, &mut self.io_options, &mut self.dithor_options);
                    spacer::small!(ui);
                    components::duplicate::call(
                        ui,
                        &mut self.io_options,
                        &mut self.duplicate_options,
                    );
                    spacer::small!(ui);
                    components::replace::call(ui, &mut self.io_options, &mut self.replace_options);
                    spacer::small!(ui);
                    components::slim::call(ui, &mut self.io_options, &mut self.slim_options);
                    spacer::small!(ui);
                    components::sort_brut::call(
                        ui,
                        &mut self.io_options,
                        &mut self.sort_brut_options,
                    );
                    spacer::small!(ui);
                    components::sort_smart::call(
                        ui,
                        &mut self.io_options,
                        &mut self.sort_smart_options,
                    );
                    spacer::small!(ui);
                    components::variable_filter::call(
                        ui,
                        &mut self.io_options,
                        &mut self.variable_filter_options,
                    );
                    spacer::small!(ui);
                    components::wrong_filter::call(
                        ui,
                        &mut self.io_options,
                        &mut self.wrong_filter_options,
                    );
                    spacer::small!(ui);
                });
            });
        });
    }
}
