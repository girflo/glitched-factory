use crate::components;
use crate::options::IoOptions;
use crate::options::SampleOptions;
use eframe::egui;

#[derive(Default)]
pub struct Sampler {
    io_options: IoOptions,
    sample_options: SampleOptions,
}

impl eframe::App for Sampler {
    fn update(&mut self, ctx: &egui::Context, _frame: &mut eframe::Frame) {
        components::io_panel::call(ctx, &mut self.io_options, false);
        components::queue_panel::call(ctx);
        components::error_panel::call(ctx, &self.io_options.error_message);

        egui::CentralPanel::default().show(ctx, |ui| {
            components::header::call(ui, "Sample algorithms");
            egui::Frame::default().inner_margin(8.0).show(ui, |ui| {
                components::sample::call(ui, &mut self.io_options, &mut self.sample_options)
            });
        });
    }
}
