use crate::components;
use crate::options::GrayscaleOptions;
use crate::options::IoOptions;
use crate::utils::spacer;
use eframe::egui;

#[derive(Default)]
pub struct Tools {
    io_options: IoOptions,
    grayscale_options: GrayscaleOptions,
}

impl eframe::App for Tools {
    fn update(&mut self, ctx: &egui::Context, _frame: &mut eframe::Frame) {
        components::io_panel::call(ctx, &mut self.io_options, true);
        components::queue_panel::call(ctx);
        components::error_panel::call(ctx, &self.io_options.error_message);

        egui::CentralPanel::default().show(ctx, |ui| {
            components::header::call(ui, "Tools");
            egui::Frame::default().inner_margin(8.0).show(ui, |ui| {
                components::convert::call(ui, &mut self.io_options);
                spacer::small!(ui);
                components::grayscale::call(ui, &mut self.io_options, &mut self.grayscale_options)
            });
        });
    }
}
