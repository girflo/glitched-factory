use crate::components;
use crate::utils::{spacer, ui_helper, UPDATE_STATUS};
use eframe::egui;
use egui::Ui;

#[derive(Default)]
pub struct Help {}

impl eframe::App for Help {
    fn update(&mut self, ctx: &egui::Context, _frame: &mut eframe::Frame) {
        egui::CentralPanel::default().show(ctx, |ui| {
            egui::ScrollArea::both().show(ui, |ui| {
                components::header::call(ui, "Help");
                egui::Frame::default().inner_margin(8.0).show(ui, |ui| {
                    egui::CollapsingHeader::new("Queue")
                        .default_open(false)
                        .show(ui, |ui| queue_help(ui));
                    spacer::small!(ui);
                    egui::CollapsingHeader::new("Glitcher")
                        .default_open(false)
                        .show(ui, |ui| glitcher_help(ui));
                    spacer::small!(ui);
                    egui::CollapsingHeader::new("Sampler")
                        .default_open(false)
                        .show(ui, |ui| sampler_help(ui));
                    spacer::small!(ui);
                    egui::CollapsingHeader::new("Tools")
                        .default_open(false)
                        .show(ui, |ui| tools_help(ui));
                    spacer::small!(ui);
                    egui::CollapsingHeader::new("About")
                        .default_open(true)
                        .show(ui, |ui| about_help(ui));
                });
            });
        });
    }
}

fn queue_help(ui: &mut Ui) {
    ui.label("The queue is not really a queue, because a queue is supposed to handle one task at a time. In the previous version of this software, it was the case. Since the version 0.3.0 multiple tasks are handled at the same time. This panel shows you the current state of the tasks you started. ");
    ui.label("Glitching entire folders can take a lot of time, so every job started will be visible in the panel. Each line represents a task you started, newer tasks are displayed at the top of the panel.");
    ui.label("A line is composed of three elements :");
    ui.label("⚫ The id of task: a number to help you keep track of a specific task");
    ui.label("⚫ The kind of task: algorithm name, sample or convert");
    ui.label("⚫ The state of the task: ongoing or finished");
}

fn glitcher_help(ui: &mut Ui) {
    ui_helper::subheader!(ui, "Input");
    ui.label("This section is for selecting the image(s) that you want to glitch. The Glitched Factory can only glitch PNG images.");
    ui.label("To select an image to glitch click on Open file... and inside of the new windows browse to the specific image and open it.");
    ui.label("If you want to use the same glitch on multiple images you can create a directory containing all the PNG images and click on Open folder, select the directory in the new windows and open it.");
    ui.label("Only the direct children are used, so if this folder contains other folder they will be ignored.");
    spacer::small!(ui);
    ui_helper::subheader!(ui, "Output");
    ui.label("This section is for specifying the where to write the newly glitched image. To specify the folder where you want to write your new image, click on Output directory and select this directory.");
    ui.label("When selected the output folder path will be displayed.");
    ui.label("The filename defines the name of the output: the index is a number and is followed by the value of the field. When glitching entire folder the files are read in alphabetical order and the output keeps this order.");
    spacer::small!(ui);
    ui_helper::subheader!(ui, "Algorithms - Brush");
    ui.label("Copy a pixel color in diagonal.");
    ui.label("Brush direction: the direction of the brush.");
    ui.label("Probability: the probability on which each pixel is brushed.");
    ui.label("Min: the minimum amount of pixel brushed at a time.");
    ui.label("Max: the maximum amount of pixel brushed at a time.");
    spacer::small!(ui);
    ui_helper::subheader!(ui, "Algorithms - Dithor");
    ui.label("Dithor is a filling algorithms I recently created, it's inspired by dithering and pixel art technics.");
    ui.label("Type:");
    ui.label("⚫ Black & White: fill the entire output image with either black or white pixels.");
    ui.label("⚫ Colors: Use only the brightest and darkest pixel of the square of 5x5 or 15x15 pixels to fill the output image");
    ui.label("⚫ Resolutions: low resolution groups 15x15 pixel high resolution groups 5x5 pixels");
    ui.hyperlink_to(
        "Dithor's project page (for more information)",
        "https://duckpuck.net/projects/dithor/",
    );
    spacer::small!(ui);
    ui_helper::subheader!(ui, "Algorithms - Duplicate");
    ui.label("For each occurrence this algorithm will copy data from another part of the image, then force the paying to use a given filter.");
    ui.label("Filter: Png filter used when writing the image");
    ui.label("Repetition: Number of times the duplication happens, the distance between the repetition is always the same.");
    ui.label("Gap: Initial offset for the first duplication.");
    spacer::small!(ui);
    ui_helper::subheader!(ui, "Algorithms - Replace");
    ui.label("For each occurrence this algorithm will replace the data with 0, then force the paying to use a given filter.");
    ui.label("Filter: Png filter used when writing the image");
    ui.label("Repetition: Number of times the replacing happens, the distance between the repetition is always the same.");
    spacer::small!(ui);
    ui_helper::subheader!(ui, "Algorithms - Sort (brut)");
    ui.label("Sort the pixels of the image according to a given attribute. When using the brut sort then entire line or column will be sorted.");
    ui.label("Direction: the direction of the sorting, either left to right either top to bottom.");
    spacer::small!(ui);
    ui_helper::subheader!(ui, "Algorithms - Sort (smart)");
    ui.label("Sort the pixels of the image according to a given attribute. When using the smart sort groups of colors is created and their content is sorted separately.");
    ui.label(
        "Inverted sorting: inverse the sorting (for example: light to dark become dark to light).",
    );
    ui.label("Direction: the direction of the sorting, either left to right either top to bottom.");
    ui.label("Sort by:");
    ui.label("⚫ Hue: the pixel with the bigger hue will be at the beginning and the lower hue on the end.");
    ui.label("⚫ Saturation: the pixel with the bigger saturation will be at the beginning and the lower saturation at the end.");
    ui.label("Detection type: the attribute on which the smart detection’s groups are created.");
    ui.label("Lightness Ranges: for smart detection using lightness range as a type. Determine which range need to be sorted (from 0 to 100).");
    spacer::small!(ui);
    ui_helper::subheader!(ui, "Algorithms - Slim");
    ui.label("Take a pixel color and copy it the one or more pixels in a direction.");
    ui.label("Slim direction: the direction on which the pixel color is copied.");
    ui.label("Probability's area of effect:");
    ui.label("⚫ Global: all color has the same probability to be slimed.");
    ui.label("⚫ Per color: each color has a given probability to be slimed.");
    ui.label("Probability: the probability in percent for each pixel to be slimed.");
    ui.label("Affected colors: the colors that are slimed, when a color is unchecked a pixel with this color is never copied to the next one.");
    ui_helper::subheader!(ui, "Algorithms - Wrong filter");
    ui.label("Force the use of a single filter");
    ui.label("Filter: Png filter used when writing the image");
    ui_helper::subheader!(ui, "Algorithms - Variable filter");
    ui.label("Force the use of a different filter for each lines of the image");
    ui.label("Type:");
    ui.label("⚫ Random: for each line a random filter is picked");
    ui.label(
        "⚫ Sequential: loop through the following sequence a filter : none, sub, up, avg, paeth",
    );
}

fn sampler_help(ui: &mut Ui) {
    ui.label("The sampler screen helps you to apply all algorithms with default values to on the specific image.");
    ui.label("This is the perfect way to create a sample for one image to see which kind of aesthetic you want to explore.");
    ui.label("The default values of each algorithm are the one predefines in the glitcher tab.");
    spacer::small!(ui);
    ui_helper::subheader!(ui, "Input");
    ui.label("As for the glitcher screen this section is for selecting the image you want to glitch. This time only one PNG image can be selected.");
    spacer::small!(ui);
    ui_helper::subheader!(ui, "Output");
    ui.label("This section again is very similar than the glitcher screen.");
    ui.label("The only difference is for the Schema for output filename, the sample will append to the file name you chose the algorithm and filter or direction used for creating the image. That way you can easily know which output has been generated with which algorithm.");
    spacer::small!(ui);
    ui_helper::subheader!(ui, "Algorithms");
    ui.label("Select which algorithm to use in this sample, only checked algorithms will be used.Select which algorithms to use in this sample, only the checked one will be used.");
}

fn tools_help(ui: &mut Ui) {
    ui_helper::subheader!(ui, "Converter");
    ui.label("A very minimal JPG to PNG converter");
    ui.label("The input and output section is similar to the Glitcher tab, the only difference is the input file format. Here JPG image or folder containing JPEG images should be used.");
    spacer::small!(ui);
    ui_helper::subheader!(ui, "Grayscale");
    ui.label("Transform the input into a grayscaled version of the image");
}

fn about_help(ui: &mut Ui) {
    ui.hyperlink_to("Project website", "https://glitchedfactory.com/");
    spacer::small!(ui);
    const VERSION: &str = env!("CARGO_PKG_VERSION");
    ui.label(format!("Version: {}", VERSION));
    spacer::small!(ui);
    if UPDATE_STATUS.new_version {
        spacer::medium!(ui);
        ui_helper::subheader!(ui, "Update available");
        ui.label(format!(
            "You can download the version {} of this software on this page:",
            UPDATE_STATUS.latest_version
        ));
        ui.hyperlink_to(
            "https://glitchedfactory.com/software/",
            "https://glitchedfactory.com/software/",
        );
        spacer::medium!(ui);
        spacer::small!(ui);
    }
    ui.label(
            "This software is open source, you can access the code or informed us if you found a bug at the following address :",
        );
    ui.hyperlink_to(
        "   gitlab.com/png-glitch/glitched-factory",
        "https://gitlab.com/png-glitch/glitched-factory",
    );
    spacer::small!(ui);
    ui.label("The glitched factory has been created using the following tools");
    ui.hyperlink_to(
        "  • rust programming language",
        "https://www.rust-lang.org/",
    );
    ui.hyperlink_to("  • egui", "https://www.egui.rs");
    ui.hyperlink_to("  • image crate", "https://docs.rs/image/latest/image/");
    ui.hyperlink_to(
        "  • lazy_static crate",
        "https://docs.rs/lazy_static/latest/lazy_static/",
    );
    ui.hyperlink_to("  • rfd crate", "https://docs.rs/rfd/latest/rfd/");
    ui.hyperlink_to("  • emacs", "https://www.gnu.org/software/emacs/");
    spacer::small!(ui);
    ui.label("If you enjoyed this tool and used it in your projects feel free to tag us on our social media pages, we would love to see how you incorporate it in your workflow :");
    ui.hyperlink_to(
        " • instagram",
        "https://www.instagram.com/glitched_factory/",
    );
    ui.hyperlink_to(" • mastodon", "https://mastodon.social/@radoteur");
}
