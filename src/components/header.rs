use crate::utils::spacer;
use eframe::egui;
use egui::Color32;
use egui_extras::{Size, StripBuilder};

pub fn call(ui: &mut egui::Ui, title: &str) {
    let dark_mode = ui.visuals().dark_mode;
    let faded_color = ui.visuals().window_fill();
    let faded_color = |color: Color32| -> Color32 {
        use egui::Rgba;
        let t = if dark_mode { 0.95 } else { 0.8 };
        egui::lerp(Rgba::from(color)..=Rgba::from(faded_color), t).into()
    };

    StripBuilder::new(ui)
        .size(Size::exact(40.0))
        .vertical(|mut strip| {
            strip.cell(|ui| {
                ui.painter().rect_filled(
                    ui.available_rect_before_wrap(),
                    0.0,
                    faded_color(Color32::GRAY),
                );
                spacer::medium!(ui);
                ui.heading(format!("  {}", title));
            });
        });
}
