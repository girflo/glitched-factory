use crate::components;
use crate::options::IoOptions;
use crate::utils::spacer;

use eframe::egui;

pub fn call(ui: &mut egui::Ui, io_options: &mut IoOptions, show_filename: bool) -> egui::Response {
    let (_rect, mut response) = ui.allocate_at_least(egui::vec2(0.0, 0.0), egui::Sense::click());
    components::header::call(ui, "Output");

    egui::Frame::default().inner_margin(8.0).show(ui, |ui| {
        if ui
            .button("🗁 Select folder")
            .on_hover_text("All glitched images will be stored in this folder")
            .clicked()
        {
            if let Some(path) = rfd::FileDialog::new().pick_folder() {
                io_options.output_path = Some(path.display().to_string());
                response.mark_changed();
            }
        }
        spacer::medium!(ui);

        if let Some(output_path) = &io_options.output_path {
            egui::CollapsingHeader::new("Picked output")
                .default_open(true)
                .show(ui, |ui| {
                    ui.label(output_path);
                });
            spacer::medium!(ui);
        }

        if show_filename {
            ui.label("Filename");
            ui.horizontal(|ui| {
                ui.add(
                    egui::TextEdit::singleline(&mut io_options.output_filename).desired_width(120.0),
                );
                if io_options.directory_input {
                    ui.label("_index.png").on_hover_text("The index is a number from 1 to x where x is the number of images in the input folder");
                } else {
                    ui.label(".png");
                }
            });
        }
    });

    response
}
