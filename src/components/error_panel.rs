use eframe::egui;
use egui::text::LayoutJob;
use egui::{ecolor::*, *};

pub fn call(ctx: &egui::Context, error_message: &String) -> () {
    if !error_message.is_empty() {
        let frame = egui::Frame::none().fill(Color32::from_rgb(128, 32, 32));
        let mut errors = LayoutJob::default();
        errors.append(
            error_message,
            0.0,
            TextFormat {
                ..Default::default()
            },
        );
        egui::TopBottomPanel::bottom("errors")
            .frame(frame)
            .show(ctx, |ui| {
                ui.add_space(2.0);
                ui.vertical_centered(|ui| {
                    ui.label(errors);
                });
            });
    }
}
