use crate::options::DithorOptions;
use crate::options::IoOptions;
use crate::utils::spacer;
use eframe::egui;
use std::thread;

pub fn call(
    ui: &mut egui::Ui,
    io_options: &mut IoOptions,
    options: &mut DithorOptions,
) -> egui::Response {
    let (_rect, mut response) = ui.allocate_at_least(egui::vec2(0.0, 0.0), egui::Sense::click());
    ui.collapsing("Dithor", |ui| {
        egui::Grid::new("my_grid")
            .num_columns(2)
            .spacing([60.0, 10.0])
            .striped(false)
            .show(ui, |ui| {
                let selected_text = if options.colors {
                    "Colors"
                } else {
                    "Black & White"
                };
                ui.label("Type")
                    .on_hover_text("Define if the output should use colors or not");
                egui::ComboBox::from_label("")
                    .selected_text(selected_text)
                    .show_ui(ui, |ui| {
                        ui.selectable_value(&mut options.colors, false, "Black & White");
                        ui.selectable_value(&mut options.colors, true, "Colors");
                    });
                ui.end_row();

                let resolution_selected_text = if options.high_res { "High" } else { "Low" };
                ui.label("Resolution")
                    .on_hover_text("Define the resolution of the dithering");
                egui::ComboBox::from_label(" ")
                    .selected_text(resolution_selected_text)
                    .show_ui(ui, |ui| {
                        ui.selectable_value(&mut options.high_res, true, "High");
                        ui.selectable_value(&mut options.high_res, false, "Low");
                    });
                ui.end_row();
            });
        spacer::small!(ui);

        if ui.button("Start dithor").clicked() {
            if io_options.check() {
                let mut io_options_clone = io_options.clone();
                let mut options_clone = options.clone();
                thread::spawn(move || options_clone.glitch(&mut io_options_clone));
            } else {
                response.mark_changed();
            }
        }
    });
    response
}
