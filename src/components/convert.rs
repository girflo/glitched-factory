use crate::options::IoOptions;
use crate::utils::spacer;
use crate::utils::MAINQUEUE;
use arko;
use eframe::egui;
use std::path::Path;
use std::thread;
use std::time::{SystemTime, UNIX_EPOCH};

pub fn call(ui: &mut egui::Ui, io_options: &mut IoOptions) -> egui::Response {
    let (_rect, mut response) = ui.allocate_at_least(egui::vec2(0.0, 0.0), egui::Sense::click());
    ui.collapsing("Convert", |ui| {
        ui.label("Convert jpg into png");
        spacer::small!(ui);
        if ui.button("Start convertion").clicked() {
            if io_options.check() {
                let mut io_options_clone = io_options.clone();
                thread::spawn(move || convert(&mut io_options_clone));
            } else {
                response.mark_changed();
            }
        }
    });
    response
}

fn convert(io_options: &mut IoOptions) {
    let id = MAINQUEUE.lock().unwrap().next_id();
    MAINQUEUE.lock().unwrap().add_task(id, "Convert");
    let overwrite = &mut (io_options.clone()).overwrite;
    let input = io_options.multiple_input(true);
    let mut counter: u32 = 0;
    for i in &input {
        let string_counter: String = format!("{:0>3}", counter.to_string());
        let output = io_options.multiple_output(&string_counter);
        let output_exist = Path::new(&output).exists();
        if *overwrite || !output_exist {
            arko::convert(&i, &output, *overwrite);
        } else {
            match SystemTime::now().duration_since(UNIX_EPOCH) {
                Ok(n) => {
                    let timestamp = n.as_secs();
                    let filename_tail = format!("{}{}", timestamp, ".png");
                    let output_name = str::replace(&output, ".png", &filename_tail);
                    arko::convert(&i, &output_name, *overwrite);
                }
                Err(_) => panic!("SystemTime before UNIX EPOCH!"),
            }
        }

        counter += 1;
    }
    MAINQUEUE.lock().unwrap().finish_task(id);
}
