use crate::options::{Directions, IoOptions, SortBrutOptions};
use crate::utils::spacer;
use eframe::egui;
use std::thread;

pub fn call(
    ui: &mut egui::Ui,
    io_options: &mut IoOptions,
    options: &mut SortBrutOptions,
) -> egui::Response {
    let (_rect, mut response) = ui.allocate_at_least(egui::vec2(0.0, 0.0), egui::Sense::click());
    ui.collapsing("Sort (brut)", |ui| {
        egui::Grid::new("my_grid")
            .num_columns(2)
            .spacing([60.0, 10.0])
            .striped(false)
            .show(ui, |ui| {
                ui.label("Direction")
                    .on_hover_text("Direction of the pixel sorting");
                egui::ComboBox::from_label("")
                    .selected_text(format!("{:?}", &mut options.direction))
                    .show_ui(ui, |ui| {
                        ui.selectable_value(
                            &mut options.direction,
                            Directions::BottomToTop,
                            "Bottom to top",
                        );
                        ui.selectable_value(
                            &mut options.direction,
                            Directions::LeftToRight,
                            "Left to right",
                        );
                        ui.selectable_value(
                            &mut options.direction,
                            Directions::RightToLeft,
                            "Right to left",
                        );
                        ui.selectable_value(
                            &mut options.direction,
                            Directions::TopToBottom,
                            "Top to bottom",
                        );
                    });
                ui.end_row();
            });
        spacer::small!(ui);

        if ui.button("Start sort").clicked() {
            if io_options.check() {
                let mut io_options_clone = io_options.clone();
                let mut options_clone = options.clone();
                thread::spawn(move || options_clone.glitch(&mut io_options_clone));
            } else {
                response.mark_changed();
            }
        }
    });
    response
}
