use crate::components;
use crate::options::IoOptions;
use crate::utils::{spacer, ui_helper};
use eframe::egui;

pub fn call(ctx: &egui::Context, ui: &mut egui::Ui, io_options: &mut IoOptions) -> egui::Response {
    use egui::*;

    let (_rect, mut response) = ui.allocate_at_least(egui::vec2(0.0, 0.0), egui::Sense::click());

    components::header::call(ui, "Input");
    egui::Frame::default().inner_margin(8.0).show(ui, |ui| {
        egui::Grid::new("my_grid")
            .num_columns(3)
            .spacing([6.0, 0.0])
            .striped(false)
            .show(ui, |ui| {
                if ui
                    .button("🗁 Open folder")
                    .on_hover_text("All png images of the folder are used as input")
                    .clicked()
                {
                    if let Some(path) = rfd::FileDialog::new().pick_folder() {
                        io_options.input_path = Some(path.display().to_string());
                        io_options.directory_input = true;
                        response.mark_changed();
                    }
                }

                ui.label(" ― or ―");

                if ui
                    .button("🖻 Open file")
                    .on_hover_text("Select a single image to be used as input")
                    .clicked()
                {
                    if let Some(path) = rfd::FileDialog::new().pick_file() {
                        io_options.input_path = Some(path.display().to_string());
                        io_options.directory_input = false;
                        response.mark_changed();
                    }
                }
                ui.end_row();
            });

        if let Some(input_path) = &io_options.input_path {
            ui_helper::picked_input!(ui, input_path);
        } else if !io_options.dropped_files.is_empty() {
            spacer::medium!(ui);
            ui.collapsing("Picked input", |ui| {
                for file in &mut io_options.dropped_files {
                    let mut info = if let Some(path) = &file.path {
                        path.display().to_string()
                    } else if !file.name.is_empty() {
                        file.name.clone()
                    } else {
                        "???".to_owned()
                    };
                    if let Some(bytes) = &file.bytes {
                        info += &format!(" ({} bytes)", bytes.len());
                    }
                    ui.label(&info);
                    let png_string = ui_helper::png_image_string(&info);
                    if png_string.is_some() {
                        ui.label(png_string.unwrap());
                    }
                }
            });
        }

        if !ctx.input(|i| i.raw.hovered_files.is_empty()) {
            let text = ctx.input(|i| {
                let mut text = "Dropping files:\n".to_owned();
                for file in &i.raw.hovered_files {
                    if let Some(path) = &file.path {
                        text += &format!("\n{}", path.display());
                    } else if !file.mime.is_empty() {
                        text += &format!("\n{}", file.mime);
                    } else {
                        text += "\n???";
                    }
                }
                text
            });

            let painter =
                ctx.layer_painter(LayerId::new(Order::Foreground, Id::new("file_drop_target")));

            let screen_rect = ctx.screen_rect();
            painter.rect_filled(screen_rect, 0.0, Color32::from_black_alpha(192));
            painter.text(
                screen_rect.center(),
                Align2::CENTER_CENTER,
                text,
                TextStyle::Heading.resolve(&ctx.style()),
                Color32::WHITE,
            );
        }

        // Collect dropped files:
        ctx.input(|i| {
            if !i.raw.dropped_files.is_empty() {
                io_options.dropped_files = i.raw.dropped_files.clone();
                io_options.input_path = None;
            }
        });
    });

    response
}
