use crate::components;
use crate::options::IoOptions;
use eframe::egui;

pub fn call(ui: &mut egui::Ui, io_options: &mut IoOptions) -> egui::Response {
    let (_rect, response) = ui.allocate_at_least(egui::vec2(0.0, 0.0), egui::Sense::click());
    components::header::call(ui, "Settings");

    egui::Frame::default().inner_margin(8.0).show(ui, |ui| {
        ui.horizontal(|ui| {
            ui.checkbox(&mut io_options.overwrite, "Overwrite output")
              .on_hover_text("When checked: if an image already exists with the same path as the output file it will be deteled");
        });
    });
    response
}
