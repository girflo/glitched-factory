use crate::options::{IoOptions, SampleOptions};
use crate::utils::MAINQUEUE;
use arko::colors::Colors;
use eframe::egui;
use std::thread;

pub fn call(
    ui: &mut egui::Ui,
    io_options: &mut IoOptions,
    options: &mut SampleOptions,
) -> egui::Response {
    let (_rect, mut response) = ui.allocate_at_least(egui::vec2(0.0, 0.0), egui::Sense::click());
    egui::Grid::new("my_grid")
        .num_columns(2)
        .spacing([60.0, 10.0])
        .striped(false)
        .show(ui, |ui| {
            ui.checkbox(&mut options.brush, "Brush");
            ui.end_row();
            ui.checkbox(&mut options.dithor, "Dithor");
            ui.end_row();
            ui.checkbox(&mut options.duplicate, "Duplicate");
            ui.end_row();
            ui.checkbox(&mut options.replace, "Replace");
            ui.end_row();
            ui.checkbox(&mut options.slim, "Slim");
            ui.end_row();
            ui.checkbox(&mut options.sort, "Sort");
            ui.end_row();
            ui.checkbox(&mut options.variable_filter, "Variable filter");
            ui.end_row();
            ui.checkbox(&mut options.wrong_filter, "Wrong filter");
            ui.end_row();
            if ui.button("Start sample").clicked() {
                if io_options.check() {
                    let mut io_options_clone = io_options.clone();
                    let mut options_clone = options.clone();
                    thread::spawn(move || sample(&mut io_options_clone, &mut options_clone));
                } else {
                    response.mark_changed();
                }
            }
        });
    response
}

fn sample(io_options: &mut IoOptions, options: &mut SampleOptions) {
    let id = MAINQUEUE.lock().unwrap().next_id();
    MAINQUEUE.lock().unwrap().add_task(id, "Sample");

    if options.brush {
        sample_brush(io_options)
    }
    if options.dithor {
        sample_dithor(io_options)
    }
    if options.duplicate {
        sample_duplicate(io_options)
    }
    if options.replace {
        sample_replace(io_options)
    }
    if options.slim {
        sample_slim(io_options)
    }
    if options.sort {
        sample_sort(io_options)
    }
    if options.variable_filter {
        sample_variable_filter(io_options)
    }
    if options.wrong_filter {
        sample_wrong_filter(io_options)
    }
    MAINQUEUE.lock().unwrap().finish_task(id);
}

fn sample_brush(io_options: &mut IoOptions) {
    let overwrite = &mut (io_options.clone()).overwrite;
    let btt = io_options.sample_output("brush_bottom_to_top.png");
    let ltr = io_options.sample_output("brush_left_to_right.png");
    let rtl = io_options.sample_output("brush_right_to_left.png");
    let ttb = io_options.sample_output("brush_top_to_bottom.png");
    let input = &mut io_options.input();
    arko::brush_bottom_to_top(&input, &btt, *overwrite, 80, 0, 20);
    arko::brush_left_to_right(&input, &ltr, *overwrite, 80, 0, 20);
    arko::brush_right_to_left(&input, &rtl, *overwrite, 80, 0, 20);
    arko::brush_top_to_bottom(&input, &ttb, *overwrite, 80, 0, 20);
}

fn sample_dithor(io_options: &mut IoOptions) {
    let overwrite = &mut (io_options.clone()).overwrite;
    let output_bw = io_options.sample_output("dithor_bw.png");
    let output_color = io_options.sample_output("dithor_color.png");
    let output_lowres_bw = io_options.sample_output("dithor_lowres_bw.png");
    let output_lowres_color = io_options.sample_output("dithor_lowres_color.png");
    let input = &mut io_options.input();
    dithor::dithor(&input, &output_bw, *overwrite, false, false);
    dithor::dithor(&input, &output_color, *overwrite, true, false);
    dithor::dithor(&input, &output_lowres_bw, *overwrite, false, true);
    dithor::dithor(&input, &output_lowres_color, *overwrite, true, true)
}

fn sample_duplicate(io_options: &mut IoOptions) {
    let overwrite = &mut (io_options.clone()).overwrite;
    let output = io_options.sample_output("duplicate");
    let input = &mut io_options.input();
    pnglitcher::duplicate_sample(&input, &output, *overwrite, 1, 0)
}

fn sample_replace(io_options: &mut IoOptions) {
    let overwrite = &mut (io_options.clone()).overwrite;
    let output = io_options.sample_output("replace");
    let input = &mut io_options.input();
    pnglitcher::replace_sample(&input, &output, *overwrite, 1)
}

fn sample_wrong_filter(io_options: &mut IoOptions) {
    let overwrite = &mut (io_options.clone()).overwrite;
    let output = io_options.sample_output("wrong_filter");
    let input = &mut io_options.input();
    pnglitcher::wrong_filter_sample(&input, &output, *overwrite)
}

fn sample_variable_filter(io_options: &mut IoOptions) {
    let overwrite = &mut (io_options.clone()).overwrite;
    let output_random = io_options.sample_output("variable_filter_random");
    let output_sequential = io_options.sample_output("variable_filter_sequential");
    let input = &mut io_options.input();
    pnglitcher::variable_filter(&input, &output_random, *overwrite, true);
    pnglitcher::variable_filter(&input, &output_sequential, *overwrite, true)
}

fn sample_slim(io_options: &mut IoOptions) {
    let overwrite = &mut (io_options.clone()).overwrite;
    let btt = io_options.sample_output("slim_bottom_to_top.png");
    let ltr = io_options.sample_output("slim_left_to_right.png");
    let rtl = io_options.sample_output("slim_right_to_left.png");
    let ttb = io_options.sample_output("slim_top_to_bottom.png");
    let colors = vec![
        Colors::Black,
        Colors::Blue,
        Colors::Cyan,
        Colors::Green,
        Colors::Grey,
        Colors::Magenta,
        Colors::Red,
        Colors::White,
        Colors::Yellow,
    ];
    let input = &mut io_options.input();
    arko::slim_bottom_to_top_global(&input, &btt, *overwrite, 80, colors.clone());
    arko::slim_left_to_right_global(&input, &ltr, *overwrite, 80, colors.clone());
    arko::slim_right_to_left_global(&input, &rtl, *overwrite, 80, colors.clone());
    arko::slim_top_to_bottom_global(&input, &ttb, *overwrite, 80, colors.clone());
}

fn sample_sort(io_options: &mut IoOptions) {
    let overwrite = &mut (io_options.clone()).overwrite;
    let btt = io_options.sample_output("sort_bottom_to_top.png");
    let ltr = io_options.sample_output("sort_left_to_right.png");
    let rtl = io_options.sample_output("sort_right_to_left.png");
    let ttb = io_options.sample_output("sort_top_to_bottom.png");
    let input = &mut io_options.input();
    arko::sort_bottom_to_top(&input, &btt, *overwrite, 0, 0, 20, true, 60, 90, 0);
    arko::sort_left_to_right(&input, &ltr, *overwrite, 0, 0, 20, true, 60, 90, 0);
    arko::sort_right_to_left(&input, &rtl, *overwrite, 0, 0, 20, true, 60, 90, 0);
    arko::sort_top_to_bottom(&input, &ttb, *overwrite, 0, 0, 20, true, 60, 90, 0);
}
