use crate::options::{Filters, IoOptions, ReplaceOptions};
use crate::utils::spacer;
use eframe::egui;
use std::thread;

pub fn call(
    ui: &mut egui::Ui,
    io_options: &mut IoOptions,
    options: &mut ReplaceOptions,
) -> egui::Response {
    let (_rect, mut response) = ui.allocate_at_least(egui::vec2(0.0, 0.0), egui::Sense::click());
    ui.collapsing("Replace", |ui| {
        egui::Grid::new("my_grid")
            .num_columns(2)
            .spacing([60.0, 10.0])
            .striped(false)
            .show(ui, |ui| {
                ui.label("Filter")
                    .on_hover_text("Filter to use in the new image");

                egui::ComboBox::from_label("")
                    .selected_text(format!("{:?}", &mut options.filter))
                    .show_ui(ui, |ui| {
                        ui.selectable_value(&mut options.filter, Filters::None, "None");
                        ui.selectable_value(&mut options.filter, Filters::Sub, "Sub");
                        ui.selectable_value(&mut options.filter, Filters::Up, "Up");
                        ui.selectable_value(&mut options.filter, Filters::Avg, "Avg");
                        ui.selectable_value(&mut options.filter, Filters::Paeth, "Paeth");
                    });
                ui.end_row();

                ui.label("Repetition")
                    .on_hover_text("Define the number of columns between two repetition");
                ui.add(egui::Slider::new(&mut options.occurence, 100..=1));
                ui.end_row();
            });
        spacer::small!(ui);

        if ui.button("Start replace").clicked() {
            if io_options.check() {
                let mut io_options_clone = io_options.clone();
                let mut options_clone = options.clone();
                thread::spawn(move || options_clone.glitch(&mut io_options_clone));
            } else {
                response.mark_changed();
            }
        }
    });
    response
}
