use crate::components;
use crate::options;
use crate::utils::spacer;
use eframe::egui;

pub fn call(
    ctx: &egui::Context,
    mut io_options: &mut options::IoOptions,
    multiple_input: bool,
) -> () {
    egui::SidePanel::left("io_panel")
        .max_width(400.0)
        .show(ctx, |ui| {
            egui::ScrollArea::vertical().show(ui, |ui| {
                spacer::mini!(ui);
                if multiple_input {
                    components::input_picker::call(ctx, ui, &mut io_options);
                } else {
                    components::single_input_picker::call(ui, &mut io_options);
                }
                spacer::inter_io!(ui);
                components::output_picker::call(ui, &mut io_options, multiple_input);
                spacer::inter_io!(ui);
                components::settings::call(ui, &mut io_options);
                spacer::medium!(ui);
            });
        });
}
