use crate::components;
use crate::utils::spacer;
use crate::utils::MAINQUEUE;
use eframe::egui;
use egui_extras::{Column, TableBuilder};

pub fn call(ctx: &egui::Context) -> () {
    egui::SidePanel::right("queue_panel")
        .max_width(400.0)
        .min_width(240.0)
        .show(ctx, |ui| {
            let tasks = MAINQUEUE.lock().unwrap().all_sorted_tasks();
            egui::ScrollArea::both().show(ui, |ui| {
                spacer::small!(ui);
                components::header::call(ui, "Queue");
                egui::Frame::default().inner_margin(8.0).show(ui, |ui| {
                    if tasks.len() == 0 {
                        ui.label("No tasks in the queue yet");
                    } else {
                        if ui
                            .button("🗑 Clear")
                            .on_hover_text("Remove all completed tasks from the queue")
                            .clicked()
                        {
                            MAINQUEUE.lock().unwrap().clear_queue();
                        }

                        spacer::small!(ui);
                        TableBuilder::new(ui)
                            .striped(true)
                            .column(Column::initial(30.0))
                            .column(Column::remainder())
                            .column(Column::initial(68.0))
                            .header(20.0, |mut header| {
                                header.col(|ui| {
                                    ui.label("ID");
                                });
                                header.col(|ui| {
                                    ui.label("Task");
                                });
                                header.col(|ui| {
                                    ui.label("State");
                                });
                            })
                            .body(|mut body| {
                                for task in tasks {
                                    body.row(20.0, |mut row| {
                                        row.col(|ui| {
                                            ui.label(task.0.to_string());
                                        });
                                        row.col(|ui| {
                                            ui.label(task.1.name);
                                        });
                                        row.col(|ui| {
                                            if task.1.state == "Ongoing" {
                                                ui.horizontal_wrapped(|ui| {
                                                    ui.add(egui::Spinner::new().size(10.0));
                                                    ui.label(task.1.state);
                                                });
                                            } else {
                                                ui.label(task.1.state);
                                            }
                                        });
                                    });
                                }
                            });
                    }
                });
            });
        });
}
