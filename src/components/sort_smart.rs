use crate::options::{DetectionTypes, Directions, IoOptions, SortSmartOptions, SortingTypes};
use crate::utils::spacer;
use eframe::egui;
use std::thread;

pub fn call(
    ui: &mut egui::Ui,
    io_options: &mut IoOptions,
    options: &mut SortSmartOptions,
) -> egui::Response {
    let (_rect, mut response) = ui.allocate_at_least(egui::vec2(0.0, 0.0), egui::Sense::click());
    ui.collapsing("Sort (smart)", |ui| {
        egui::Grid::new("my_grid")
            .num_columns(2)
            .spacing([60.0, 10.0])
            .striped(false)
            .show(ui, |ui| {
                ui.label("Direction")
                    .on_hover_text("Direction of the pixel sorting");
                egui::ComboBox::from_label("")
                    .selected_text(format!("{:?}", &mut options.direction))
                    .show_ui(ui, |ui| {
                        ui.selectable_value(
                            &mut options.direction,
                            Directions::BottomToTop,
                            "Bottom to top",
                        );
                        ui.selectable_value(
                            &mut options.direction,
                            Directions::LeftToRight,
                            "Left to right",
                        );
                        ui.selectable_value(
                            &mut options.direction,
                            Directions::RightToLeft,
                            "Right to left",
                        );
                        ui.selectable_value(
                            &mut options.direction,
                            Directions::TopToBottom,
                            "Top to bottom",
                        );
                    });
                ui.end_row();

                ui.label("Detection type")
                    .on_hover_text("Element used to defined how to group pixels to sort");
                egui::ComboBox::from_label(" ")
                    .selected_text(format!("{:?}", &mut options.detection_type))
                    .show_ui(ui, |ui| {
                        ui.selectable_value(
                            &mut options.detection_type,
                            DetectionTypes::Lightness,
                            "Lightness",
                        );
                        ui.selectable_value(
                            &mut options.detection_type,
                            DetectionTypes::ColorGroup,
                            "Color group",
                        );
                    });
                ui.end_row();

                ui.label("Sorting type")
                    .on_hover_text("Element used to defined the order of pixels");
                egui::ComboBox::from_label("  ")
                    .selected_text(format!("{:?}", &mut options.sorting_type))
                    .show_ui(ui, |ui| {
                        ui.selectable_value(&mut options.sorting_type, SortingTypes::Hue, "Hue");
                        ui.selectable_value(
                            &mut options.sorting_type,
                            SortingTypes::Saturation,
                            "Saturation",
                        );
                    });
                ui.end_row();

                ui.label("Min")
                    .on_hover_text("Minimum number of pixel brushed at a time for first range");
                ui.add(
                    egui::DragValue::new(&mut options.min_one)
                        .suffix(" pixels")
                        .range(0.0..=options.max_one),
                );
                ui.end_row();

                ui.label("Max")
                    .on_hover_text("Maximum number of pixel brushed at a time for first range");
                ui.add(
                    egui::DragValue::new(&mut options.max_one)
                        .suffix(" pixels")
                        .range(options.min_one..=options.min_two),
                );
                ui.end_row();

                ui.label("Min 2")
                    .on_hover_text("Minumum number of pixel brushed at a time for second range");
                ui.add(
                    egui::DragValue::new(&mut options.min_two)
                        .suffix(" pixels")
                        .range(options.max_one..=options.max_two),
                );
                ui.end_row();

                ui.label("Max 2")
                    .on_hover_text("Maximum number of pixel brushed at a time for second range");

                ui.add(
                    egui::DragValue::new(&mut options.max_two)
                        .suffix(" pixels")
                        .range(options.min_two..=100.0),
                );
                ui.end_row();
            });
        spacer::small!(ui);

        if ui.button("Start sort").clicked() {
            if io_options.check() {
                let mut io_options_clone = io_options.clone();
                let mut options_clone = options.clone();
                thread::spawn(move || options_clone.glitch(&mut io_options_clone));
            } else {
                response.mark_changed();
            }
        }
    });
    response
}
