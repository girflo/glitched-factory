use crate::components;
use crate::options::IoOptions;
use crate::utils::{spacer, ui_helper};
use eframe::egui;

pub fn call(ui: &mut egui::Ui, io_options: &mut IoOptions) -> egui::Response {
    let (_rect, mut response) = ui.allocate_at_least(egui::vec2(0.0, 0.0), egui::Sense::click());

    components::header::call(ui, "Input");
    egui::Frame::default().inner_margin(8.0).show(ui, |ui| {
        if ui
            .button("🖻 Open file")
            .on_hover_text("Select a single image to be used as input")
            .clicked()
        {
            if let Some(path) = rfd::FileDialog::new().pick_file() {
                io_options.input_path = Some(path.display().to_string());
                response.mark_changed();
            }
        }

        if let Some(input_path) = &io_options.input_path {
            ui_helper::picked_input!(ui, input_path);
        }
    });

    response
}
