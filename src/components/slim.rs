use crate::options::{Directions, IoOptions, SlimOptions};
use crate::utils::spacer;
use eframe::egui;
use std::thread;

pub fn call(
    ui: &mut egui::Ui,
    io_options: &mut IoOptions,
    options: &mut SlimOptions,
) -> egui::Response {
    let (_rect, mut response) = ui.allocate_at_least(egui::vec2(0.0, 0.0), egui::Sense::click());
    ui.collapsing("Slim", |ui| {
        egui::Grid::new("my_grid")
            .num_columns(2)
            .spacing([60.0, 10.0])
            .striped(false)
            .show(ui, |ui| {
                ui.label("Direction").on_hover_text("Direction of the slim");
                egui::ComboBox::from_label("")
                    .selected_text(format!("{:?}", &mut options.direction))
                    .show_ui(ui, |ui| {
                        ui.selectable_value(
                            &mut options.direction,
                            Directions::BottomToTop,
                            "Bottom to top",
                        );
                        ui.selectable_value(
                            &mut options.direction,
                            Directions::LeftToRight,
                            "Left to right",
                        );
                        ui.selectable_value(
                            &mut options.direction,
                            Directions::RightToLeft,
                            "Right to left",
                        );
                        ui.selectable_value(
                            &mut options.direction,
                            Directions::TopToBottom,
                            "Top to bottom",
                        );
                    });
                ui.end_row();

                ui.label("Probability")
                    .on_hover_text("Probability for a pixel to be slimmed");
                ui.add(egui::Slider::new(&mut options.probability, 0.0..=100.0).suffix("%"));
                ui.end_row();
            });

        egui::Grid::new("colors")
            .num_columns(3)
            .spacing([60.0, 10.0])
            .striped(false)
            .show(ui, |ui| {
                ui.checkbox(&mut options.black.check, "Black");
                ui.checkbox(&mut options.grey.check, "Grey");
                ui.checkbox(&mut options.white.check, "White");
                ui.end_row();
                ui.checkbox(&mut options.red.check, "Red");
                ui.checkbox(&mut options.blue.check, "Blue");
                ui.checkbox(&mut options.green.check, "Green");
                ui.end_row();
                ui.checkbox(&mut options.cyan.check, "Cyan");
                ui.checkbox(&mut options.magenta.check, "Magenta");
                ui.checkbox(&mut options.yellow.check, "Yellow");
                ui.end_row();
            });

        spacer::small!(ui);

        if ui.button("Start slim").clicked() {
            if io_options.check() {
                let mut io_options_clone = io_options.clone();
                let mut options_clone = options.clone();
                thread::spawn(move || options_clone.glitch(&mut io_options_clone));
            } else {
                response.mark_changed();
            }
        }
    });
    response
}
