use crate::options::{IoOptions, VariableFilterOptions};
use crate::utils::spacer;
use eframe::egui;
use std::thread;

pub fn call(
    ui: &mut egui::Ui,
    io_options: &mut IoOptions,
    options: &mut VariableFilterOptions,
) -> egui::Response {
    let (_rect, mut response) = ui.allocate_at_least(egui::vec2(0.0, 0.0), egui::Sense::click());
    ui.collapsing("Variable filter", |ui| {
        egui::Grid::new("my_grid")
            .num_columns(2)
            .spacing([60.0, 10.0])
            .striped(false)
            .show(ui, |ui| {
                let selected_text = if options.random {
                    "Random"
                } else {
                    "Sequential"
                };
                ui.label("Type")
                    .on_hover_text("Define how the filter is selected for each lines of the image");
                egui::ComboBox::from_label(" ")
                    .selected_text(selected_text)
                    .show_ui(ui, |ui| {
                        ui.selectable_value(&mut options.random, false, "Sequential");
                        ui.selectable_value(&mut options.random, true, "Random");
                    });
                ui.end_row();
            });
        spacer::small!(ui);

        if ui.button("Start variable filter").clicked() {
            if io_options.check() {
                let mut io_options_clone = io_options.clone();
                let mut options_clone = options.clone();
                thread::spawn(move || options_clone.glitch(&mut io_options_clone));
            } else {
                response.mark_changed();
            }
        }
    });
    response
}
