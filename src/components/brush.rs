use crate::options::{BrushOptions, Directions, IoOptions};
use crate::utils::spacer;
use eframe::egui;
use std::thread;

pub fn call(
    ui: &mut egui::Ui,
    io_options: &mut IoOptions,
    options: &mut BrushOptions,
) -> egui::Response {
    let (_rect, mut response) = ui.allocate_at_least(egui::vec2(0.0, 0.0), egui::Sense::click());
    ui.collapsing("Brush", |ui| {
        egui::Grid::new("my_grid")
            .num_columns(2)
            .spacing([60.0, 10.0])
            .striped(false)
            .show(ui, |ui| {
                ui.label("Direction")
                    .on_hover_text("Direction of the brush");
                egui::ComboBox::from_label("")
                    .selected_text(format!("{:?}", &mut options.direction))
                    .show_ui(ui, |ui| {
                        ui.selectable_value(
                            &mut options.direction,
                            Directions::BottomToTop,
                            "Bottom to top",
                        );
                        ui.selectable_value(
                            &mut options.direction,
                            Directions::LeftToRight,
                            "Left to right",
                        );
                        ui.selectable_value(
                            &mut options.direction,
                            Directions::RightToLeft,
                            "Right to left",
                        );
                        ui.selectable_value(
                            &mut options.direction,
                            Directions::TopToBottom,
                            "Top to bottom",
                        );
                    });
                ui.end_row();

                ui.label("Probability")
                    .on_hover_text("Probability for a pixel to be brushed");
                ui.add(egui::Slider::new(&mut options.probability, 0.0..=100.0).suffix("%"));
                ui.end_row();

                ui.label("Min")
                    .on_hover_text("Minimum number of pixel brushed at a time");
                ui.add(
                    egui::DragValue::new(&mut options.min)
                        .suffix(" pixels")
                        .range(0.0..=options.max),
                );
                ui.end_row();

                ui.label("Max")
                    .on_hover_text("Maximum number of pixel brushed at a time");

                ui.add(
                    egui::DragValue::new(&mut options.max)
                        .suffix(" pixels")
                        .range(options.min..=100.0),
                );
            });
        spacer::small!(ui);

        if ui.button("Start brush").clicked() {
            if io_options.check() {
                let mut io_options_clone = io_options.clone();
                let mut options_clone = options.clone();
                thread::spawn(move || options_clone.glitch(&mut io_options_clone));
            } else {
                response.mark_changed();
            }
        }
    });
    response
}
