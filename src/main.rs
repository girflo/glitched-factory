#![cfg_attr(not(debug_assertions), windows_subsystem = "windows")] // hide console window on Windows in release

#[macro_use]
extern crate lazy_static;
mod components;
mod options;
mod tabs;
mod utils;
use crate::utils::{spacer, MAINQUEUE, UPDATE_STATUS};
use eframe::egui;
use egui::FontFamily::Proportional;
use egui::FontId;
use egui::TextStyle::*;
use std::collections::BTreeMap;

#[derive(Default)]
struct Tabs {
    glitcher: crate::tabs::Glitcher,
    sampler: crate::tabs::Sampler,
    tools: crate::tabs::Tools,
    help: crate::tabs::Help,
}

impl Tabs {
    fn iter_mut(&mut self) -> impl Iterator<Item = (&str, &str, &mut dyn eframe::App)> {
        vec![
            (
                "Glitcher",
                "Glitcher",
                &mut self.glitcher as &mut dyn eframe::App,
            ),
            (
                "Sampler",
                "Sampler",
                &mut self.sampler as &mut dyn eframe::App,
            ),
            ("Tools", "Tools", &mut self.tools as &mut dyn eframe::App),
            ("ℹ Help", "Help", &mut self.help as &mut dyn eframe::App),
        ]
        .into_iter()
    }
}

#[derive(Default)]
struct GlitchedFactory {
    tabs: Tabs,
    selected_anchor: String,
}

impl eframe::App for GlitchedFactory {
    fn update(&mut self, ctx: &egui::Context, frame: &mut eframe::Frame) {
        if self.selected_anchor.is_empty() {
            self.selected_anchor = self.tabs.iter_mut().next().unwrap().0.to_owned();
        }

        egui::TopBottomPanel::top("top_bar").show(ctx, |ui| {
            spacer::small!(ui);
            ui.horizontal_wrapped(|ui| {
                for (name, anchor, _tab) in self.tabs.iter_mut() {
                    if ui
                        .selectable_label(self.selected_anchor == anchor, name)
                        .clicked()
                    {
                        self.selected_anchor = anchor.to_owned();
                    }
                }
                egui::widgets::global_theme_preference_switch(ui);
            });
            spacer::small!(ui);
        });

        for (_name, anchor, app) in self.tabs.iter_mut() {
            if anchor == self.selected_anchor {
                app.update(ctx, frame);
            }
        }
    }
}

fn main() -> eframe::Result {
    lazy_static::initialize(&MAINQUEUE);
    lazy_static::initialize(&UPDATE_STATUS);
    let options = eframe::NativeOptions {
        viewport: egui::ViewportBuilder::default().with_drag_and_drop(true),
        ..Default::default()
    };
    let text_styles: BTreeMap<_, _> = [
        (Heading, FontId::new(20.0, Proportional)),
        (Name("Nav".into()), FontId::new(17.0, Proportional)),
        (Body, FontId::new(13.0, Proportional)),
        (Monospace, FontId::new(13.0, Proportional)),
        (Button, FontId::new(15.0, Proportional)),
        (Small, FontId::new(10.0, Proportional)),
    ]
    .into();
    eframe::run_native(
        "GlitchedFactory",
        options,
        Box::new(|cc| {
            let context = &cc.egui_ctx;
            context.all_styles_mut(move |style| style.text_styles = text_styles.clone());
            egui_extras::install_image_loaders(context);
            Ok(Box::new(GlitchedFactory::default()))
        }),
    )
}
