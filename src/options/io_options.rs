use crate::utils;
use eframe::egui;

#[derive(Clone)]
pub struct IoOptions {
    pub dropped_files: Vec<egui::DroppedFile>,
    pub error_message: String,
    pub input_path: Option<String>,
    pub output_filename: String,
    pub output_path: Option<String>,
    pub overwrite: bool,
    pub directory_input: bool,
}

impl Default for IoOptions {
    fn default() -> Self {
        Self {
            dropped_files: Vec::new(),
            error_message: "".to_string(),
            input_path: None,
            output_filename: "glitched".to_string(),
            output_path: None,
            overwrite: true,
            directory_input: false,
        }
    }
}

impl IoOptions {
    pub fn check(&mut self) -> bool {
        self.error_message = "".to_string();
        if self.output_path.is_some() {
            if self.input_path.is_some() || !self.dropped_files.is_empty() {
                true
            } else {
                self.error_message = "No input provided".to_string();
                false
            }
        } else {
            self.error_message = "No output provided".to_string();
            false
        }
    }

    pub fn input(&mut self) -> &str {
        self.input_path.as_ref().unwrap()
    }

    pub fn multiple_input(&mut self, convertion: bool) -> Vec<String> {
        if self.input_path.is_some() {
            let input = self.input();
            if utils::format_validator::call(input, convertion) {
                vec![self.input().to_string()]
            } else {
                utils::folder_reader::call(self.input(), convertion)
            }
        } else {
            utils::drop_reader::call(self.dropped_files.clone(), convertion)
        }
    }

    pub fn multiple_output(&mut self, counter: &str) -> String {
        if self.directory_input {
            format!(
                "{}/{}_{}{}",
                self.output_path.as_ref().unwrap(),
                self.output_filename,
                counter,
                ".png"
            )
        } else {
            format!(
                "{}/{}{}",
                self.output_path.as_ref().unwrap(),
                self.output_filename,
                ".png"
            )
        }
    }

    pub fn sample_output(&mut self, filename: &str) -> String {
        format!("{}/{}", self.output_path.as_ref().unwrap(), filename)
    }
}
