#[derive(Clone)]
pub struct SampleOptions {
    pub brush: bool,
    pub dithor: bool,
    pub duplicate: bool,
    pub replace: bool,
    pub slim: bool,
    pub sort: bool,
    pub variable_filter: bool,
    pub wrong_filter: bool,
}

impl Default for SampleOptions {
    fn default() -> Self {
        Self {
            brush: true,
            dithor: true,
            duplicate: true,
            replace: true,
            slim: true,
            sort: true,
            variable_filter: true,
            wrong_filter: true,
        }
    }
}
