use crate::options::IoOptions;
use crate::utils::glitcher;
use crate::utils::MAINQUEUE;
use std::path::Path;
use std::time::{SystemTime, UNIX_EPOCH};

#[derive(Clone)]
pub struct GrayscaleOptions {
    pub name: String,
}

impl Default for GrayscaleOptions {
    fn default() -> Self {
        Self {
            name: "Grayscale".to_string(),
        }
    }
}

impl GrayscaleOptions {
    pub fn glitch(&mut self, io_options: &mut IoOptions) {
        glitcher::glitch!(io_options, self);
    }

    fn call_glitch_algo(&mut self, input: &str, output: &str, _overwrite: bool) {
        image::open(input)
            .unwrap()
            .grayscale()
            .save(output)
            .unwrap();
    }
}
