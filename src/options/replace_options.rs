use crate::options::Filters;
use crate::options::IoOptions;
use crate::utils::glitcher;
use crate::utils::MAINQUEUE;
use std::path::Path;
use std::time::{SystemTime, UNIX_EPOCH};

#[derive(Clone)]
pub struct ReplaceOptions {
    pub name: String,
    pub filter: Filters,
    pub occurence: u8,
}

impl Default for ReplaceOptions {
    fn default() -> Self {
        Self {
            name: "Replace".to_string(),
            filter: Filters::Paeth,
            occurence: 1,
        }
    }
}

impl ReplaceOptions {
    pub fn glitch(&mut self, io_options: &mut IoOptions) {
        glitcher::glitch!(io_options, self);
    }

    fn call_glitch_algo(&mut self, input: &str, output: &str, overwrite: bool) {
        pnglitcher::replace(
            input,
            output,
            overwrite,
            self.filter as u8,
            self.occurence as usize,
        );
    }
}
