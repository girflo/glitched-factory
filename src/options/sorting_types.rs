#[derive(Copy, Clone, Debug, PartialEq)]
pub enum SortingTypes {
    Hue = 0,
    Saturation = 1,
}
