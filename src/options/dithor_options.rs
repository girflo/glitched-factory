use crate::options::IoOptions;
use crate::utils::glitcher;
use crate::utils::MAINQUEUE;
use std::path::Path;
use std::time::{SystemTime, UNIX_EPOCH};

#[derive(Clone)]
pub struct DithorOptions {
    pub name: String,
    pub colors: bool,
    pub high_res: bool,
}

impl Default for DithorOptions {
    fn default() -> Self {
        Self {
            name: "Dithor".to_string(),
            colors: false,
            high_res: true,
        }
    }
}

impl DithorOptions {
    pub fn glitch(&mut self, io_options: &mut IoOptions) {
        glitcher::glitch!(io_options, self);
    }

    fn call_glitch_algo(&mut self, input: &str, output: &str, overwrite: bool) {
        dithor::dithor(input, output, overwrite, self.colors, self.high_res);
    }
}
