#[derive(Copy, Clone, Debug, PartialEq)]
pub enum DetectionTypes {
    Lightness = 0,
    ColorGroup = 1,
}
