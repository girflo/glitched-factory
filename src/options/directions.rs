#[derive(Clone, Debug, PartialEq)]
pub enum Directions {
    BottomToTop,
    LeftToRight,
    RightToLeft,
    TopToBottom,
}
