#[derive(Copy, Clone, Debug, PartialEq)]
pub enum Filters {
    None = 0,
    Sub = 1,
    Up = 2,
    Avg = 3,
    Paeth = 4,
}
