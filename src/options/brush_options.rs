use crate::options::Directions;
use crate::options::IoOptions;
use crate::utils::glitcher;
use crate::utils::MAINQUEUE;
use std::path::Path;
use std::time::{SystemTime, UNIX_EPOCH};

#[derive(Clone)]
pub struct BrushOptions {
    pub name: String,
    pub direction: Directions,
    pub max: f32,
    pub min: f32,
    pub probability: f32,
}

impl Default for BrushOptions {
    fn default() -> Self {
        Self {
            name: "Brush".to_string(),
            direction: Directions::BottomToTop,
            max: 20.0,
            min: 0.0,
            probability: 80.0,
        }
    }
}

impl BrushOptions {
    pub fn glitch(&mut self, io_options: &mut IoOptions) {
        glitcher::glitch!(io_options, self);
    }

    fn call_glitch_algo(&mut self, input: &str, output: &str, overwrite: bool) {
        match self.direction {
            Directions::BottomToTop => arko::brush_bottom_to_top(
                input,
                output,
                overwrite,
                self.probability as i32,
                self.min as i32,
                self.max as i32,
            ),
            Directions::LeftToRight => arko::brush_left_to_right(
                input,
                output,
                overwrite,
                self.probability as i32,
                self.min as i32,
                self.max as i32,
            ),
            Directions::RightToLeft => arko::brush_right_to_left(
                input,
                output,
                overwrite,
                self.probability as i32,
                self.min as i32,
                self.max as i32,
            ),
            _ => arko::brush_top_to_bottom(
                input,
                output,
                overwrite,
                self.probability as i32,
                self.min as i32,
                self.max as i32,
            ),
        }
    }
}
