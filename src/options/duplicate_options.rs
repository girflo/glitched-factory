use crate::options::Filters;
use crate::options::IoOptions;
use crate::utils::glitcher;
use crate::utils::MAINQUEUE;
use std::path::Path;
use std::time::{SystemTime, UNIX_EPOCH};

#[derive(Clone)]
pub struct DuplicateOptions {
    pub name: String,
    pub filter: Filters,
    pub occurence: u8,
    pub gap: u8,
}

impl Default for DuplicateOptions {
    fn default() -> Self {
        Self {
            name: "Duplicate".to_string(),
            filter: Filters::Paeth,
            occurence: 1,
            gap: 0,
        }
    }
}

impl DuplicateOptions {
    pub fn glitch(&mut self, io_options: &mut IoOptions) {
        glitcher::glitch!(io_options, self);
    }

    fn call_glitch_algo(&mut self, input: &str, output: &str, overwrite: bool) {
        pnglitcher::duplicate(
            input,
            output,
            overwrite,
            self.filter as u8,
            self.occurence as usize,
            self.gap as usize,
        );
    }
}
