use crate::options::Filters;
use crate::options::IoOptions;
use crate::utils::glitcher;
use crate::utils::MAINQUEUE;
use std::path::Path;
use std::time::{SystemTime, UNIX_EPOCH};

#[derive(Clone)]
pub struct WrongFilterOptions {
    pub name: String,
    pub filter: Filters,
}

impl Default for WrongFilterOptions {
    fn default() -> Self {
        Self {
            name: "Wrong filter".to_string(),
            filter: Filters::Paeth,
        }
    }
}

impl WrongFilterOptions {
    pub fn glitch(&mut self, io_options: &mut IoOptions) {
        glitcher::glitch!(io_options, self);
    }

    fn call_glitch_algo(&mut self, input: &str, output: &str, overwrite: bool) {
        pnglitcher::wrong_filter(input, output, overwrite, self.filter as u8);
    }
}
