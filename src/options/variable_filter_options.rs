use crate::options::IoOptions;
use crate::utils::glitcher;
use crate::utils::MAINQUEUE;
use std::path::Path;
use std::time::{SystemTime, UNIX_EPOCH};

#[derive(Clone)]
pub struct VariableFilterOptions {
    pub name: String,
    pub random: bool,
}

impl Default for VariableFilterOptions {
    fn default() -> Self {
        Self {
            name: "Variable filter".to_string(),
            random: false,
        }
    }
}

impl VariableFilterOptions {
    pub fn glitch(&mut self, io_options: &mut IoOptions) {
        glitcher::glitch!(io_options, self);
    }

    fn call_glitch_algo(&mut self, input: &str, output: &str, overwrite: bool) {
        pnglitcher::variable_filter(input, output, overwrite, self.random);
    }
}
