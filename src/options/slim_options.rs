use crate::options::Directions;
use crate::options::IoOptions;
use crate::utils::glitcher;
use crate::utils::MAINQUEUE;
use arko::colors::Colors;
use std::path::Path;
use std::time::{SystemTime, UNIX_EPOCH};

#[derive(Clone)]
pub struct SlimOptions {
    pub name: String,
    pub direction: Directions,
    pub probability: f32,
    pub black: SlimColor,
    pub blue: SlimColor,
    pub cyan: SlimColor,
    pub green: SlimColor,
    pub grey: SlimColor,
    pub magenta: SlimColor,
    pub red: SlimColor,
    pub white: SlimColor,
    pub yellow: SlimColor,
}

impl Default for SlimOptions {
    fn default() -> Self {
        Self {
            name: "Slim".to_string(),
            direction: Directions::BottomToTop,
            probability: 80.0,
            black: SlimColor::default(),
            blue: SlimColor::default(),
            cyan: SlimColor::default(),
            green: SlimColor::default(),
            grey: SlimColor::default(),
            magenta: SlimColor::default(),
            red: SlimColor::default(),
            white: SlimColor::default(),
            yellow: SlimColor::default(),
        }
    }
}

impl SlimOptions {
    pub fn colors(&mut self) -> Vec<Colors> {
        let mut colors = Vec::new();
        if self.black.check {
            colors.push(Colors::Black);
        }
        if self.blue.check {
            colors.push(Colors::Blue);
        }
        if self.cyan.check {
            colors.push(Colors::Cyan);
        }
        if self.green.check {
            colors.push(Colors::Green);
        }
        if self.grey.check {
            colors.push(Colors::Grey);
        }
        if self.magenta.check {
            colors.push(Colors::Magenta);
        }
        if self.red.check {
            colors.push(Colors::Red);
        }
        if self.white.check {
            colors.push(Colors::White);
        }
        if self.yellow.check {
            colors.push(Colors::Yellow);
        }
        colors
    }
}

#[derive(Clone, Hash)]
pub struct SlimColor {
    pub check: bool,
    pub probability: i32,
}

impl Default for SlimColor {
    fn default() -> Self {
        Self {
            check: true,
            probability: 80,
        }
    }
}

impl SlimOptions {
    pub fn glitch(&mut self, io_options: &mut IoOptions) {
        glitcher::glitch!(io_options, self);
    }

    fn call_glitch_algo(&mut self, input: &str, output: &str, overwrite: bool) {
        match self.direction {
            Directions::BottomToTop => arko::slim_bottom_to_top_global(
                input,
                output,
                overwrite,
                self.probability as i32,
                self.colors(),
            ),
            Directions::LeftToRight => arko::slim_left_to_right_global(
                input,
                output,
                overwrite,
                self.probability as i32,
                self.colors(),
            ),
            Directions::RightToLeft => arko::slim_right_to_left_global(
                input,
                output,
                overwrite,
                self.probability as i32,
                self.colors(),
            ),
            _ => arko::slim_top_to_bottom_global(
                input,
                output,
                overwrite,
                self.probability as i32,
                self.colors(),
            ),
        }
    }
}
