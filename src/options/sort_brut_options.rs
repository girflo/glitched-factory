use crate::options::Directions;
use crate::options::IoOptions;
use crate::utils::glitcher;
use crate::utils::MAINQUEUE;
use std::path::Path;
use std::time::{SystemTime, UNIX_EPOCH};

#[derive(Clone)]
pub struct SortBrutOptions {
    pub name: String,
    pub direction: Directions,
}

impl Default for SortBrutOptions {
    fn default() -> Self {
        Self {
            name: "Sort brut".to_string(),
            direction: Directions::BottomToTop,
        }
    }
}

impl SortBrutOptions {
    pub fn glitch(&mut self, io_options: &mut IoOptions) {
        glitcher::glitch!(io_options, self);
    }

    fn call_glitch_algo(&mut self, input: &str, output: &str, overwrite: bool) {
        match self.direction {
            Directions::BottomToTop => arko::sort_brut_bottom_to_top(input, output, overwrite),
            Directions::LeftToRight => arko::sort_brut_left_to_right(input, output, overwrite),
            Directions::RightToLeft => arko::sort_brut_right_to_left(input, output, overwrite),
            _ => arko::sort_brut_top_to_bottom(input, output, overwrite),
        }
    }
}
