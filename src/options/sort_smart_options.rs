use crate::options::DetectionTypes;
use crate::options::Directions;
use crate::options::IoOptions;
use crate::options::SortingTypes;
use crate::utils::glitcher;
use crate::utils::MAINQUEUE;
use std::path::Path;
use std::time::{SystemTime, UNIX_EPOCH};

#[derive(Clone)]
pub struct SortSmartOptions {
    pub name: String,
    pub direction: Directions,
    pub detection_type: DetectionTypes,
    pub max_one: f32,
    pub max_two: f32,
    pub min_one: f32,
    pub min_two: f32,
    pub multiple_range: bool,
    pub sorting_type: SortingTypes,
}

impl Default for SortSmartOptions {
    fn default() -> Self {
        Self {
            name: "Sort smart".to_string(),
            direction: Directions::BottomToTop,
            detection_type: DetectionTypes::Lightness,
            max_one: 20.0,
            max_two: 90.0,
            min_one: 0.0,
            min_two: 60.0,
            multiple_range: true,
            sorting_type: SortingTypes::Hue,
        }
    }
}

impl SortSmartOptions {
    pub fn glitch(&mut self, io_options: &mut IoOptions) {
        glitcher::glitch!(io_options, self);
    }

    fn call_glitch_algo(&mut self, input: &str, output: &str, overwrite: bool) {
        match self.direction {
            Directions::BottomToTop => arko::sort_bottom_to_top(
                input,
                output,
                overwrite,
                self.detection_type as i32,
                self.min_one as i32,
                self.max_one as i32,
                self.multiple_range,
                self.min_two as i32,
                self.max_two as i32,
                self.sorting_type as i32,
            ),
            Directions::LeftToRight => arko::sort_left_to_right(
                input,
                output,
                overwrite,
                self.detection_type as i32,
                self.min_one as i32,
                self.max_one as i32,
                self.multiple_range,
                self.min_two as i32,
                self.max_two as i32,
                self.sorting_type as i32,
            ),
            Directions::RightToLeft => arko::sort_right_to_left(
                input,
                output,
                overwrite,
                self.detection_type as i32,
                self.min_one as i32,
                self.max_one as i32,
                self.multiple_range,
                self.min_two as i32,
                self.max_two as i32,
                self.sorting_type as i32,
            ),
            _ => arko::sort_top_to_bottom(
                input,
                output,
                overwrite,
                self.detection_type as i32,
                self.min_one as i32,
                self.max_one as i32,
                self.multiple_range,
                self.min_two as i32,
                self.max_two as i32,
                self.sorting_type as i32,
            ),
        }
    }
}
